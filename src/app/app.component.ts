import { Component } from '@angular/core';

@Component({
  selector: 'at-root',
  template: '<at-view></at-view>',
  styles: ['']
})
export class AppComponent {
}
