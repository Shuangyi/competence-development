import { Component } from '@angular/core';

@Component({
  selector: 'at-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent {

  newUser: User = {
    name: '',
    title: '',
    experience: ''
  };

  constructor() { }

  createUser() {
    console.log('Creating user...');
  }

}

interface User {
  name: string;
  title: string;
  experience: string;
}

