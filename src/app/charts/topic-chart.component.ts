import { Component } from '@angular/core';

import { Options } from 'highcharts';

import { MatrixConfigService } from '../config/matrix-config.service';
import { ChartConfigService } from '../config/chart-config.service';


@Component({
  selector: 'at-topic-chart',
  template: `<mat-card><chart [options]="options"></chart></mat-card>`
})
export class TopicChartComponent {

  options: Options = {
    chart: {
      type: 'column',
      backgroundColor: this.chartConfig.backgroundColor
    },
    title: {
      text: 'Competency matrix',
      style: { color: this.chartConfig.titleColor }
    },
    subtitle: {
      text: 'All employees'
    },
    xAxis: {
      categories: this.matrixModelService.areas,
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Level'
      }
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    credits: {
      enabled: false
    },
    legend: this.chartConfig.legend,
    series: [{
      name: 'Woodhouse',
      data: [5, 2, 4, 1, 2, 3]

    }, {
      name: 'Snoopy',
      data: [5, 3, 4, 3, 1, 1]

    }, {
      name: 'Lucas',
      data: [3, 4, 1, 5, 4, 3]

    }, {
      name: 'Charlie',
      data: [5, 1, 3, 4, 1, 3]
    }]
  };

  constructor(private matrixModelService: MatrixConfigService, private chartConfig: ChartConfigService) { }
}


