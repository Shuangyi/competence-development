import { Component } from '@angular/core';

import { Options } from 'highcharts';

import { MatrixConfigService } from '../config/matrix-config.service';
import { ChartConfigService } from '../config/chart-config.service';


@Component({
  selector: 'at-polar-chart',
  template: `<mat-card><chart [options]="options"></chart></mat-card>`
})
export class PolarChartComponent {

  options: Options = {
    chart: {
      polar: true,
      type: 'line',
      backgroundColor: this.chartConfig.backgroundColor
    },
    title: {
      text: 'Competency matrix',
      style: { color: this.chartConfig.titleColor }
    },
    subtitle: {
      text: 'All employees'
    },
    credits: {
      enabled: false
    },
    legend: this.chartConfig.legend,
    xAxis: {
      categories: this.matrixModelService.areas,
      tickmarkPlacement: 'on',
      lineWidth: 0
    },
    yAxis: {
      lineWidth: 0,
      min: 1,
      max: 5
    },
    series: [{
      name: 'Woodhouse',
      data: [5, 4, 5, 4, 5, 3]
    },
    {
      name: 'Snoopy',
      data: [3, 1, 1, 1, 2, 3]
    }]
  };

  constructor(private matrixModelService: MatrixConfigService, private chartConfig: ChartConfigService) { }
}


