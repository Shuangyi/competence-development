import { Component, Inject, OnInit, Optional } from '@angular/core';

import { NAVIGATION_CONFIG, NavigationConfig } from '../config/config.module';

@Component({
  selector: 'at-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  components = [];

  constructor( @Optional() @Inject(NAVIGATION_CONFIG) private navigationConfig: NavigationConfig[]) { }

  private componetsContainsCategory(category: string) {
    let i = this.components.length;
    while (i--) {
      if (this.components[i].category === category) {
        return true;
      }
    }
    return false;
  }

  private buildNavBar() {
    for (let conf of this.navigationConfig) {
      if (!this.componetsContainsCategory(conf.category)) {
        this.components.push({ category: conf.category });
      }
    }

    for (let conf of this.navigationConfig) {
      let i = this.components.length;
      while (i--) {
        if (this.components[i].category === conf.category) {
          this.components.splice(i + 1, 0, {
            path: conf.path,
            icon: conf.icon,
            title: conf.title
          });
        }
      }
    }
  }

  ngOnInit() {
    this.buildNavBar();
  }
}
