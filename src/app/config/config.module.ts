import { NgModule, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PolarChartComponent } from '../charts/polar-chart.component';
import { TopicChartComponent } from '../charts/topic-chart.component';
import { CreateMatrixComponent } from '../create-matrix/create-matrix.component';
import { CreateUserComponent } from '../create-user/create-user.component';
import { ChartConfigService } from './chart-config.service';
import { MatrixConfigService } from './matrix-config.service';

export const NAVIGATION_CONFIG = new InjectionToken('navigation-config');

export interface NavigationConfig {
  component: any;
  category: string;
  title: string;
  icon: string;
  path: string;
}

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ChartConfigService,
    MatrixConfigService,
    {
      provide: NAVIGATION_CONFIG,
      // Add new compontents here, along with info like path (url), title and icon
      // The first component is loaded as a homepage
      useValue: [
        {
          component: CreateMatrixComponent,
          category: 'Matrices',
          title: 'Create matrix',
          path: 'create-matrix',
          icon: 'build'
        },
        {
          component: PolarChartComponent,
          category: 'Charts',
          title: 'Polar chart',
          path: 'polar-chart',
          icon: 'face'
        },
        {
          component: TopicChartComponent,
          category: 'Charts',
          title: 'Topic chart',
          path: 'topic-chart',
          icon: 'face'
        },
        {
          component: CreateUserComponent,
          category: 'Users',
          title: 'Create user',
          path: 'create-user',
          icon: 'android'
        }]
    }]
})
export class ConfigModule {

}
