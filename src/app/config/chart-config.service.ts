import { Injectable } from '@angular/core';

@Injectable()
export class ChartConfigService {

  // This is just for common styles that charts share

  backgroundColor = '#424242';
  titleColor = '#D8D8D8';

  legend = {
    itemStyle: {
      color: '#D8D8D8'
    },
    itemHiddenStyle: {
      color: '#D8D8D8'
    },
    itemHoverStyle: {
      color: '#FFFFFF'
    }
  };
}
