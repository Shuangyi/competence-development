import { Injectable } from '@angular/core';

@Injectable()
export class MatrixConfigService {

  // This is the general configuration of the matrix

  areas = [
    'Medication', 'Access log', 'Overview',
    'Bed management', 'Theatre management', 'Care documentation'
  ];

  levels = [1, 2, 3, 4, 5];

}
