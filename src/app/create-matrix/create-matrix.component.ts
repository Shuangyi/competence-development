import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';

import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { MatrixConfigService } from '../config/matrix-config.service';


@Component({
  selector: 'at-create-matrix',
  templateUrl: './create-matrix.component.html',
  styleUrls: ['./create-matrix.component.css']
})
export class CreateMatrixComponent {

  employees: Employee[] = [{
    name: 'Charlie',
    matrix: this.createMatrix()
  },
  {
    name: 'Snoopy',
    matrix: this.createMatrix()
  },
  {
    name: 'Woodhouse',
    matrix: this.createMatrix()
  }];

  emptyMatrix = this.createMatrix();


  constructor(private matrixModelService: MatrixConfigService) {
    this.createMatrix();
  }

  saveMatrix() {
    console.log('Saving...');
  }

  private createMatrix(): Row[] {
    const matrix: Row[] = [];
    for (let area of this.matrixModelService.areas) {
      matrix.push({
        area: area,
        levels: this.matrixModelService.levels,
        selectedLevel: null
      });
    }
    return matrix;
  }
}

interface Row {
  area: string;
  levels: number[];
  selectedLevel: number;
}

interface Employee {
  name: string;
  matrix: Row[];
}
