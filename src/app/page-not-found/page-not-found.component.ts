import { Component } from '@angular/core';

@Component({
  selector: 'at-page-not-found',
  template: `<mat-card>Page not found!</mat-card>`,
  styles: ['']
})
export class PageNotFoundComponent {

  constructor() { }
}
