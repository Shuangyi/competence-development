import { NgModule, InjectionToken, Inject } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, Router, Route } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { ChartModule } from 'angular2-highcharts';

import { ViewComponent } from './view.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { MaterialModule } from '../material.module';
import { CreateMatrixComponent } from '../create-matrix/create-matrix.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { PolarChartComponent } from '../charts/polar-chart.component';
import { TopicChartComponent } from '../charts/topic-chart.component';
import { CreateUserComponent } from '../create-user/create-user.component';
import { ConfigModule, NavigationConfig, NAVIGATION_CONFIG } from '../config/config.module';
import { ChartConfigService } from '../config/chart-config.service';

@NgModule({
  // Add new compontents here...
  entryComponents: [
    CreateUserComponent,
    CreateMatrixComponent,
    PolarChartComponent,
    TopicChartComponent,
    PageNotFoundComponent
  ],
  // ...and here
  declarations: [
    PolarChartComponent,
    TopicChartComponent,
    CreateUserComponent,
    CreateMatrixComponent,
    PageNotFoundComponent,
    ViewComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ChartModule.forRoot(require('highcharts')),
    ConfigModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([])
  ],
  exports: [
    ViewComponent
  ],
  providers: [
    ChartConfigService,
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    }
  ]
})
export class ViewModule {

  private routes: Routes = [];

  constructor(router: Router, @Inject(NAVIGATION_CONFIG) private navigationConfig: NavigationConfig[]) {
    this.buildRoutes();
    router.resetConfig([...router.config, ...this.routes]);
  }

  private buildRoutes() {
    for (let conf of this.navigationConfig) {
      this.routes.push(
        { path: conf.path, component: conf.component }
      );
    }
    this.routes.push({ path: '', redirectTo: this.routes[0].path, pathMatch: 'full' });
    this.routes.push({ path: '**', component: PageNotFoundComponent });
  }
}

declare let require: any;
export function highchartsFactory() {
  const hc = require('highcharts');
  const dd = require('highcharts/modules/drilldown');
  const hcm = require('highcharts/highcharts-more.js');
  dd(hc);
  hcm(hc);
  return hc;
}
